 '''
 define a calculator class for other functions to use
 '''
 class Calculator:
    def __init__(self):
        self.value = 0

    def add(self, val):
        self.value += val

    def sub(self, val):
        self.value -= val

    def mul(self, val):
        self.value *= val
    
    def div(self, val):
        self.value /= val

    def sqrt(self, val):
        self.value = val ** (1/2)

    def pow(self, val):
        self.value = val ** val

    def sin(self, val):
        self.value = sin(val)

    def cos(self, val):
        self.value = cos(val)